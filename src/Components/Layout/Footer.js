import React from "react";
import styles from './Layout.module.scss';

export default function Footer() {
  return (
    <div className={styles.footer}>
      <div className={styles.up}>
        <div className={styles.public}>
          <div className={styles.title1}>Public</div>
          <div className={styles.publicContent}>
            <div>
              <a href="/">Acasa</a>
            </div>
            <div>
              <a href="/stagii">Stagii</a>
            </div>
          </div>
        </div>

        <div className={styles.contact}>
          <div className={styles.title2}>Contact</div>
          <div className={styles.contactContent}>
              <a href="/">Facebook</a>
            </div>
            <div>
              <a href="/">Mail</a>
            </div>
            <div>
              <a href="/">Telefon</a>
            </div>
        </div>
      </div>

      <div className={styles.termeni}>
          <div>
            <a href="/termeni-si-conditii">Termeni si conditii</a>
          </div>
          <div>
            <a href="/politica-de-confidentialitate">Politica de confidentialitate</a>
          </div>
        </div>
    </div>
  );
}
