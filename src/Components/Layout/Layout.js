import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import styles from "./Layout.module.scss";

export default function Layout(props) {
  return (
    <div className={styles.App}>
        <Header/>
        <div className={styles.content}>
          {props.children}
        </div>
        <Footer/>
    </div>
  );
}
