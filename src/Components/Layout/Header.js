import React from "react";
import styles from "./Layout.module.scss";

export default function Header() {
  return (
    <div className={styles.header}>
      <div className={styles.logo}>
        <p>Stagii UPB</p>
      </div>
      <div className={styles.links}>
        <a className={styles.link} href='/'>Acasa</a>
        <a className={styles.link} href='/stagii'>Stagii</a>
      </div>
      <div className={styles.buttons}>
        <button className={styles.button}>Inregistrare</button>
        <button className={styles.button}>Autentificare</button>
      
        <p>RO</p>
      </div>
    </div>
  );
}
