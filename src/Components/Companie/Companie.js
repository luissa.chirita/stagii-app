import React from "react";
import styles from './Companie.module.scss';
import adobe from "./adobe.jpg";

export default function Companie() {
  return (
    <div className={styles.company}>
        <div className={styles.firstLine}>
          <div className={styles.firstColumn}>
            <img src={adobe} alt={'adobe'}/>
          </div>
          <div className={styles.secondColumn}>
            <div className={styles.title}> ADOBE SYSTEMS ROMANIA</div>
            <div className={styles.companyInfo}>
              <div>Anchor Plaza, Bd. Timisoara 26Z, Bucuresti 061331</div>
              <div>adoberomania@gmail.com</div>
              <div>0756327267</div>
            </div>
          </div>
        </div>

        <div className={styles.secondLine}>
          <p> Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea.Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea.
          </p>
        </div>

        <div className={styles.thirdLine}>
          <div>Stagii deschise </div>
        </div>
    </div>
  );
}
