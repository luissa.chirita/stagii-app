import React from "react";
import styles from './Student.module.scss';
import photo from "./student.jpg";

export default function Companie() {
  return (
    <div className={styles.container}>
        <div className={styles.firstLine}>
          <div className={styles.firstColumn}>
            <img src={photo} alt={'student'} class="profilePicture"/>
          </div>
          <div className={styles.secondColumn}>
            <div className={styles.title}>CHIRITA MARIA-LUISSA</div>
            <div className={styles.companyInfo}>
              <div>CV Link</div>
              <div>luissa.chirita@gmail.com</div>
              <div>0756327267</div>
              <div className={styles.keyWords}>
                  <button className={styles.button}>#C++</button>
                  <button className={styles.button}>#C</button>
                  <button className={styles.button}>#Java</button>
              </div>
            </div>
          </div>
        </div>

        <div className={styles.secondLine}>
          <p> Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea.Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea. Experiențele minunate au puterea să inspire, să transforme și să impulsioneze lumea să avanseze. Iar fiecare experiență excelentă începe cu creativitatea.
          </p>
        </div>

        <div className={styles.thirdLine}>
          <div>Stagii deschise </div>
        </div>
    </div>
  );
}
