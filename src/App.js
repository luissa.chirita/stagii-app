import React, { Component, useState, createContext} from "react";
import ReactDOM from "react-dom";
import Layout from "./Components/Layout/Layout";
import Home from "./Components/Home/Home";
import Stagii from "./Components/Stagii/Stagii";
import Companie from "./Components/Companie/Companie";
import Student from "./Components/Student/Student";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function App() {
  return (
    <Layout>
        <Router>
          <div>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/stagii" exact component={Stagii} />
              <Route path="/companie" exact component={Companie} />
              <Route path="/student" exact component={Student} />
            </Switch>
          </div>
        </Router>
    </Layout>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
